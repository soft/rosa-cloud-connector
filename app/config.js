{
    "providers": [
        {
            "name" : "4shared",
            "title" : "<a href='http://www.4shared.com/'>4Shared</a>",
            "url" : "https://webdav.4shared.com/"
        },
        {
            "name" : "dropbox",
            "title" : "<a href='http://www.dropbox.com/'>Dropbox</a>",
            "url" : "",
            "hasClient" : true,
            "folder" : "Dropbox"
        },
        {
            "name" : "egnyte",
            "title" : "<a href='https://www.egnyte.com/'>Egnyte</a>",
            "url" : "https://webdav-username.egnyte.com/username-egnyte"
        },
        {
            "name" : "yandex_disk",
            "title" : "<a href='http://disk.yandex.ru/'>Yandex_Disk</a>",
            "url" : "https://webdav.yandex.ru"
        },
        {
            "name" : "spideroak",
            "title" : "<a href='http://spideroak.com/'>SpiderOak</a>",
            "url" : "",
            "hasClient" : true,
            "folder" : "SpiderOak Hive"
        }

    ]
}
