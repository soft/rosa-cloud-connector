#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
            QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);

    QTranslator myappTranslator;
    myappTranslator.load("rosa_cloud_" + QLocale::system().name(), "/usr/share/rosa-cloud-connector/data/lang/" );
    a.installTranslator(&myappTranslator);

    a.setOrganizationName("RosaCloud");
    a.setApplicationName("rosa-cloud");
    a.setApplicationDisplayName("Rosa Cloud");
    MainWindow w;
    w.setAttribute(Qt::WA_QuitOnClose);
    w.show();
    w.postInit();

    return a.exec();
}
