<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AuthDialog</name>
    <message>
        <location filename="authdialog.ui" line="14"/>
        <location filename="ui_authdialog.h" line="107"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="30"/>
        <location filename="ui_authdialog.h" line="108"/>
        <source>Login:</source>
        <translation>Учетное имя:</translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="45"/>
        <location filename="ui_authdialog.h" line="109"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="66"/>
        <location filename="ui_authdialog.h" line="110"/>
        <source>Unmount</source>
        <translation>Отмонтировать</translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="76"/>
        <location filename="ui_authdialog.h" line="111"/>
        <source>Mount</source>
        <translation>Смонтировать</translation>
    </message>
    <message>
        <location filename="authdialog.cpp" line="33"/>
        <source>Authorizing in %1</source>
        <translation>Авторизация в %1</translation>
    </message>
    <message>
        <location filename="authdialog.cpp" line="72"/>
        <source>Enter storage name</source>
        <translation>Введите имя хранилища</translation>
    </message>
    <message>
        <location filename="authdialog.cpp" line="73"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="authdialog.cpp" line="76"/>
        <location filename="authdialog.cpp" line="105"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="authdialog.cpp" line="76"/>
        <source>Error installing Egnyte!</source>
        <translation>Ошибка при установке Egnyte!</translation>
    </message>
    <message>
        <location filename="authdialog.cpp" line="105"/>
        <source>Authorization error for </source>
        <translation>Ошибка авторизации для</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="25"/>
        <location filename="mainwindow.ui" line="37"/>
        <location filename="ui_mainwindow.h" line="95"/>
        <location filename="ui_mainwindow.h" line="96"/>
        <source>Rosa Cloud Connector</source>
        <translation>Rosa Cloud Connector</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <location filename="mainwindow.cpp" line="145"/>
        <source>Install</source>
        <translation>Установить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="166"/>
        <source>Open folder</source>
        <translation>Открыть папку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="250"/>
        <location filename="mainwindow.cpp" line="262"/>
        <location filename="mainwindow.cpp" line="288"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="250"/>
        <source>Error downloading SpiderOak!</source>
        <translation>Ошибка при скачивании SpiderOak!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Error installing SpiderOak!</source>
        <translation>Ошибка при ecnfyjdrt SpiderOak!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Enter Root password</source>
        <translation>Введите пароль Root</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Root password:</source>
        <translation>Пароль root:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="288"/>
        <source>You have entered incorrect password three times!</source>
        <translation>Вы ввели некорректный пароль трижды!</translation>
    </message>
</context>
<context>
    <name>Spinbox</name>
    <message>
        <location filename="spinbox.ui" line="14"/>
        <location filename="ui_spinbox.h" line="49"/>
        <source>Download...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="spinbox.ui" line="26"/>
        <location filename="ui_spinbox.h" line="50"/>
        <source>text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="spinbox.ui" line="42"/>
        <location filename="ui_spinbox.h" line="51"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="spinbox.cpp" line="11"/>
        <source>Please wait, download can take several minutes</source>
        <translation>Пожалуйста, подождите. Загрузка может занять несколько минут</translation>
    </message>
</context>
</TS>
